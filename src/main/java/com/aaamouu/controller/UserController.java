package com.aaamouu.controller;

import com.aaamouu.dao.UserDao;
import com.aaamouu.model.User;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet(name = "userServlet", value = "/register")
public class UserController extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private UserDao userDao;

    public void init(){
        userDao = new UserDao();
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("/WEB-INF/views/register/register.jsp");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        register(request, response);
    }


    //
    private void register(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String firstName = request.getParameter("firstName");
        String lastName = request.getParameter("lastName");
        String userName = request.getParameter("UserName");
        String password = request.getParameter("password");

        //Creating a User Object
        User employee = new User();
        employee.setLastName(firstName);
        employee.setLastName(lastName);
        employee.setUsername(userName);
        employee.setPassword(password);

        try {
            int result = userDao.registerEmployee(employee);
            if(result==1){
                request.setAttribute("NOTIFICATION", "User Registered successfully !");
            }
        }catch (Exception e){
            e.printStackTrace();
        }

        RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/views/register/register.jsp");
        dispatcher.forward(request, response);
    }
}
